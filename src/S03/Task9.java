package S03;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество строк: ");
        int numbers = scanner.nextInt();
        scanner.nextLine();
        String[] lines = new String[numbers];
        for (int i = 0; i < numbers; i++) {
            System.out.println("Строка" + " " + i + " " + ":");
            lines[i] = scanner.nextLine();
        }
        System.out.print(String.join(", ", lines));
    }
}




