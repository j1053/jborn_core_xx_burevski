package S03;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите текст!");
        String text = scan.nextLine();
        String capitalZhy = "Жы";
        String zhy = "жы";
        String capitalShy = "Шы";
        String shy = "шы";
        String capitalZhi = "Жи";
        String zhi = "жи";
        String capitalShi = "Ши";
        String shi = "ши";
        if (text.contains(zhi) || text.contains(shi) || text.contains(capitalZhi) || text.contains(capitalShi)) {
            System.out.println("Правильно!");
        } else if (text.contains(zhy) || text.contains(shy) || text.contains(capitalShy) || text.contains(capitalZhy)) {
            System.out.println("Неправильно! Жи/Ши пиши с буквой 'и'");
        } else
            System.out.println("Для этого слова правило не применяется");
    }
}

