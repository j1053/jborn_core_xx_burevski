package S03;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        String str1;
        StringBuilder str2 = new StringBuilder();
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите строку.");
        str1 = scan.nextLine();
        int length = str1.length();
        for (int i = length - 1; i >= 0; i--)
            str2.append(str1.charAt(i));
        System.out.println("Результат инверсии: " + str2);
    }
}




