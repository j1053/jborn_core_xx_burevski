package S03;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите слово.");
        String word = scan.nextLine();
        if (word.equalsIgnoreCase(new StringBuilder(word).reverse().toString())) {
            System.out.println("Введенное вами слово является палиндромом");
        } else {
            System.out.println("Введенное вами слово не является палиндромом");
        }
    }
}




