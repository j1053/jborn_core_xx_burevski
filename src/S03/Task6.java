package S03;

import java.io.IOException;
import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите слово.");
        String word = scan.nextLine();
        System.out.println("Теперь введите любой символ из введенного слова.");
        char symbol = (char) System.in.read();
        char ch = Character.toUpperCase(symbol);
        int count = 0;
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == symbol)
                count++;
        }
        System.out.println("Количество вхождений - " + count);
        System.out.println("Преобразованная строка - " + (word.replace(symbol, ch)));
    }
}