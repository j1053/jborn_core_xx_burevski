package S02;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Пожалуйста, введите двузначное число.");
        int n = scan.nextInt();
        int decade = n / 10; // Количество десятков
        int units = n % 10; // Количетво единиц
        int sum = decade + units; // Сумма цифр
        int multiplication = decade * units; // Произведение цифр
        System.out.println("Количество десятков в вашем числе" + " " + decade);
        System.out.println("Количество единиц в вашем числе" + " " + units);
        System.out.println("Сумма ваших цифр" + " " + sum);
        System.out.println("Произведение ваших цифр" + " " + multiplication);
    }
}
