package s02;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите радиус");
        double r = scan.nextDouble();
        double square = Math.PI * (r * r);
        System.out.println("Площадь круга равна - " + square);
        double circumference = Math.PI * 2 * r;
        System.out.println("Длина окружности равна - " + circumference);
    }
}
