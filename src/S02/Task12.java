package S02;

import java.util.Scanner;

public class Task12 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите расстояние в пути (в КМ).");
        double km = scan.nextDouble();
        System.out.println("Введите ваш средний расход топлива на 100 км (в литрах).");
        double l = scan.nextDouble();
        System.out.println("Введите стоимость одного литра топлива (в рублях).");
        double rub = scan.nextDouble();
        System.out.println("Ваш расход топлива составит - " + km / l);
        System.out.println("Ваш расход денежных средств составит - " + (km / l) * rub);
    }
}
