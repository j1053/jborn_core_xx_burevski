package S02;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        int even = 0;
        int odd = 1;
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите количество элементов в массиве");
        int array = scan.nextInt();
        int[] numbers = new int[array];
        for (int i = 0; i < numbers.length; i++) {
            System.out.println("Введите" + " " + i + " " + "элемент массива");
            numbers[i] = scan.nextInt();

            if (i % 2 == 0) {
                even += numbers[i];
            } else {
                odd *= numbers[i];
            }
        }
        System.out.println("Сумма всех четных элементов массива - " + even);
        System.out.println("Произведение всех нечетных элементов массива - " + odd);
    }
}


