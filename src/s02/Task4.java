package s02;

import java.util.Scanner;
import java.lang.Math;

    public class Task4 {

        public static void main(String[] args) {
            Scanner scan = new Scanner(System.in);
            System.out.println("Пожалуйста, введите координаты перовой точки через пробел.");
            double x1 = scan.nextDouble();
            double y1 = scan.nextDouble();
            System.out.println("Теперь введите координаты второй точки через пробел.");
            double x2 = scan.nextDouble();
            double y2 = scan.nextDouble();
            System.out.println("Расстояние между вашими точками - " + " " + Math.sqrt(y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
            // Возможно нужно было добавить Исключения, но к сожалению я их еще не изучал.
        }
    }
