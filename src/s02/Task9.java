package s02;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Пожалуйста введите число, а я посчитаю сумму всех его цифр.");
        int i = scan.nextInt();
        int sum = 0;
        while (i != 0) {
            sum += i % 10;
            i /= 10;
        }
        System.out.println("Сумма ваших цифр равна -" + " " + sum);
    }
}




