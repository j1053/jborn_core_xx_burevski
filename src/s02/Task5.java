package s02;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {

        System.out.println("Здравствуйте! Введите три целых числа, а я определю являются ли они тройкой Пифагора.");
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите первое число!");
        int x = scan.nextInt();
        System.out.println("Введите второе число!");
        int y = scan.nextInt();
        System.out.println("Введите третье число!");
        int z = scan.nextInt();
        boolean b = (x * x == y * y + z * z) || (y * y == x * x + z * z) || (z * z == x * x + y * y);
        if (b) {
            System.out.println("Ваши числа являются Пифагоровой тройкой!");
        } else {
            System.out.println("Ваши числа не являются Пифагоровой тройкой");
        }
    }
}

