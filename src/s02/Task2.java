package s02;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите количество секунд.");
        int seconds = scan.nextInt();
        System.out.println("Прошло" + " " + seconds / 60 / 60 + " " + "полных часов.");
        System.out.println("Прошло" + " " + (seconds % 3600) / 60 + " " + "полных минут с начала очередного часа");
        System.out.println("Прошло" + " " + seconds % 60 + " " + "полных секунд с начала очередной минуты");
    }
}