package S02;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите число!");
        int number = scan.nextInt();
        for (int i = 1; i < 10; i++) {
            System.out.println(number + " x " + i + " = " + number * i);
        }
    }
}
